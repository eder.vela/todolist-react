import {useReducer} from 'react'
import { todoReducer } from './assets/todoReducer'
import { TodoAdd } from "./components/TodoAdd";
import { List } from "./components/List";

export const ToDoApp = () => {

    const initialState = [
        // {
        //     id: new Date().getTime(),
        //     description: 'Get soul stone',
        //     done: true
        // }
    ]

    const [todos, dispatch] = useReducer(todoReducer, initialState)

    const handleNewTodo = (todo) => {
      const action = {
        type: '[TODO] Add Todo',
        payload: todo
      }

      dispatch(action)

    }


  return (
    <div>
        <h1>To do App (10) <small>Pendientes 21</small></h1>
        <hr />
        <div className="row">
            <div className="col-7">
                <List todos={todos} />
            </div>
            <div className="col-5">
                <h4>Agregar to do</h4>
                <TodoAdd onNewTodo={todo => handleNewTodo(todo)} />
            </div>
        </div>
    </div>
  )
}
