import React from 'react'

export const todoReducer = (initialState = [], action) => {
    switch (action.type) {
        case '[TODO] Add Todo':
            // throw new Error('Action.type ABC not implemented yet')
            return [...initialState, action.payload]
    
        default:
            return initialState;
    }
}
