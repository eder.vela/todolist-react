import { ListItem } from "./ListItem";


export const List = ({todos =[]}) => {
    console.log(todos)
  return (
    <ul className='list-group'>

    {
        todos.map(
            todo=> (
                <ListItem 
                key={todo.id}
                todo={todo}
                />
            )
        )
    }
</ul>
  )
}
