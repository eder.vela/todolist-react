import React from 'react'

export const ListItem = ({todo}) => {

    const {description, id, done} = todo
  return (
    <li className='list-group-item d-flex justify-content-between' key={id}>
        <span className='align-self-center'>{description}</span>
        <button className='btn btn-danger btn-sm'>Borrar</button>
    </li>
  )
}
